//
//  TodayViewController.swift
//  RanDom Widget
//
//  Created by Mark Tyers on 26/11/2014.
//  Copyright (c) 2014 Coventry University. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding {
    
    @IBOutlet weak var randomNumber: UILabel!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view from its nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)!) {
        if let sharedDefaults = NSUserDefaults(suiteName: "group.uk.ac.coventry.mtyers.randomwidget") {
            let num = sharedDefaults.integerForKey("randomNumber")
            self.randomNumber.text = String(format: "%06d", num)
        }
        completionHandler(NCUpdateResult.NewData)
    }
    
}
