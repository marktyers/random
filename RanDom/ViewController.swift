//
//  ViewController.swift
//  RanDom
//
//  Created by Mark Tyers on 26/11/2014.
//  Copyright (c) 2014 Coventry University. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var randomNumber: UILabel!
    
    @IBAction func generate(sender: UIButton) {
        println("generate")
        let num = Int(arc4random_uniform(10000))
        println(num)
        self.randomNumber.text = String(format: "%06d", num)
        
        if let sharedDefaults = NSUserDefaults(suiteName: "group.uk.ac.coventry.mtyers.randomwidget") {
            sharedDefaults.setInteger(num, forKey: "randomNumber")
            sharedDefaults.synchronize()
            println(sharedDefaults.dictionaryRepresentation())
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let sharedDefaults = NSUserDefaults(suiteName: "group.uk.ac.coventry.mtyers.randomwidget") {
            let num = sharedDefaults.integerForKey("randomNumber")
            
            self.randomNumber.text = String(format: "%06d", num)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

